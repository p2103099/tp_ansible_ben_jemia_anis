# TP Automatisation

Réalisé par Anis BEN JEMIA

# Résultat

PHP est fonctionnel.

En ce qui concerne MySQL, j'ai fait ce que j'ai pu, l'installation n'a pas entièrement réussi, j'ai également commencé
à coder ce que j'aurais utilisé si l'installation aurait marché dans deploy-db.yml

## Commandes à exécuter

`ansible-playbook init.yml -i inventories/ --skip-tags "mysql"`

`ansible-playbook config.yml -i inventories/ --skip-tags "update,mysql"`

`ansible-playbook deploy-source.yml -i inventories/ --skip-tags "update,mysql"`

